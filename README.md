# ESP32-Covid19-spreadslower

We are a group of students at Grenoble University, currently working on an Electronic project about COVID-19.

We have seen different approaches based on smartphone tracking applications that would be useful at the end of lockdock against epidemy .

In France, many people like homeless people, elderly people, some prisoners can't use a smartphone. It is a big problem because 1. those people are the most exposed to COVID, and 2. it is important for the global efficiency of the tracking system that as many people as possible use these applications. From our COVID multi-agent simulation model , there is a bifurcation point around 80% of population equipment in the efficiency of tracking systems, and smartphone alone are probably not able to cover the threshold level. In addition, there is much concern in France about data privacy; this might discourage some additional people to use a smartphone for tracking purpose, even if it is against their own interest.

For these reasons, we want to follow a different but complementary approach: we would like to design a specific electronic portable device able to connect to other devices of the same kind, but also able to connect to a smartphone using a tracking application.

The device would only produce a “contagiousness” estimation of people we meet outside during day. When a user equipped by the device meets another one equipped either with a device or a smartphone, we measure distance, time they spend together, and number of people already met by each of them, to update their “contagiousness” estimation. This estimation can help people in the neighbourhood to get around somebody with a high estimation, but it also permits to somebody COVID-positive or exposed to high level of risk because of his/her job to signal the risk to his/her neighborhood, and thus reducing the risk of contamination.


The device we have in mind uses Peer-to-Peer connection (with for example BLE, or ESP-Now) to other devices and to smartphones. We have started to experiment with ESP32 microcontroller chips; we also have started to implement basic distance measuring and “contagiousness” estimation algorithms.

Now, we are at a state where we would like to collaborate with people developing  a tracking smartphone application to interface both kinds of devices.

We would like to build an interoperable smartphone+ESP32 COVID tracking system.


