// Libraries for SD card
#include "FS.h"
#include "SD.h"
#include <SPI.h>

// Prototype de fonction
bool ID_saved(String ID_delete);
void appendFile(fs::FS &fs, const char * path, const char * message);
//*******************************************************************SD card************************************
//**************************************************************************************************************

// Write to the SD card (DON'T MODIFY THIS FUNCTION)
void writeFile(fs::FS &fs, const char * path, const char * message) {
  Serial.printf("Writing file: %s\n", path);

  File file = fs.open(path, FILE_WRITE);
  if(!file) {
    Serial.println("Failed to open file for writing");
    return;
  }
  if(file.print(message)) {
    Serial.println("File written");
  } else {
    Serial.println("Write failed");
  }
  file.close();
}

// Define CS pin for the SD card module
#define SD_CS 5
void init_sd()
{
 // Initialize SD card 
  if(!SD.begin(SD_CS)) {
    Serial.println("Card Mount Failed");
    return;
  }
  if(SD.cardType() == CARD_NONE) {
    Serial.println("No SD card attached");
    return;
  }
  Serial.println("Initializing SD card...");
  if (!SD.begin(SD_CS)) {
    Serial.println("ERROR - SD card initialization failed!");
    return;    // init failed
  }

  // If the data.txt file doesn't exist
  // Create a file on the SD card and write the data labels
  File file = SD.open("/data.txt");
  if(!file){
    Serial.println("File doens't exist");
    Serial.println("Creating file...");
    writeFile(SD, "/data.txt", "ID, Date, Hour, Minute, ms\r\n");
  }
  else {
    Serial.println("File already exists");  
  }
  file.close();
}




// Write readings on the SD card
void logSDCard(String ID,String day,String hour,String minute, String ms) { //Si ms =0 alors on connait le temps exact, sinon day =hour=minute =0
  String dataMessage;
  dataMessage = ID + "," + day + "," + hour + ","+minute+","+ms+"\r\n";
  if(!ID_saved(ID))
  {
	
	Serial.print("Save data: ");
	Serial.println(dataMessage);
	appendFile(SD, "/data.txt", dataMessage.c_str());
  }
  else{
		File file = SD.open("/data.txt");
	  	while(file.available()){
			if(String(file.read())==ID){file.print(dataMessage);} //On remplace la Data par une autre (re-writing) on peut aussi utiliser write en théorie
		}
  }

}






// Append data to the SD card (DON'T MODIFY THIS FUNCTION)
void appendFile(fs::FS &fs, const char * path, const char * message) {
  Serial.printf("Appending to file: %s\n", path);

  File file = fs.open(path, FILE_APPEND);
  if(!file) {
    Serial.println("Failed to open file for appending");
    return;
  }
  if(file.print(message)) {
    Serial.println("Message appended");
  } else {
    Serial.println("Append failed");
  }
  file.close();
}

//Read data from SD card
void readFile(fs::FS &fs, const char * path){
    Serial.printf("Reading file: %s\n", path);

    File file = fs.open(path);
    if(!file){
        Serial.println("Failed to open file for reading");
        return;
    }

    Serial.print("Read from file: ");
    while(file.available()){
        Serial.write(file.read());
    }
    file.close();
}

bool ID_saved(String ID_delete)
{
	    File file = SD.open("/data.txt");
		while(file.available()){
			if(String(file.read())==ID_delete){return true;}
		}
		return false;
}
