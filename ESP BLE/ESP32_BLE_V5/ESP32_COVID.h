//Libraries for screen
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
//Library for Kalman filter
#include <SimpleKalmanFilter.h>






//----------------------------------------
// Affichage sur l'écran OLED--------------------------------------

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 32 // OLED display height, in pixels

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1); // Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)

void screen(float text){
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3D for 128x64
    Serial.println(F("SSD1306 allocation failed"));
    for(;;);
  }
  display.clearDisplay();

  display.setTextSize(2);
  display.setTextColor(WHITE,BLACK); // Set text to plot foreground and background colours
  display.setCursor(0, 10);
  // Display static text
  display.print("RSSI : ");
  display.print(int(text));
  display.display(); 
}

//----------------------------------------------------
// filtre pour le rssi

SimpleKalmanFilter simpleKalmanFilter(4, 3, 0.1); // SimpleKalmanFilter(e_mea, e_est, q), e_mea: Measurement Uncertainty, e_est: Estimation Uncertainty, q: Process Noise  

long filtre(long bad_RSSI){
  return simpleKalmanFilter.updateEstimate(bad_RSSI);
}

//----------------------------------------------------
// distance

double distance(long filtered_RSSI) { // la distance est fausse
  long CalibratedRssi = -48; //A vérifier cette valeur
  return pow(10.0, (double)(-CalibratedRssi + filtered_RSSI) / -20.0); //CalibratedRssi est le RSSI mesuré pour 1m 
  //En intérieur :pow(10.0, (double)(CalibratedRssi - filtered_RSSI) / 20.0);
  //En extérieur : pow(10.0, (double)(CalibratedRssi - filtered_RSSI) / 10.0);
}
