#include "SD_card.h"
//*********************************************** Social distanciation: application logic *********************
//*************************************************************************************************************

long surround_size = 0;

String surround_names[100];
long surround_RSSIs[100];
long surround_is_suspect[100];
long surround_timestamp[100];
long surround_last_timestamp[100];

long total_meet_count = 0;  // meets counter
double dist_min = 0.5;    // minimum distance over which meets counter is incremented

long remove_duration = 5000; // time after which a user who has not sent any notification is removed from surrounding in ms

//Emits an audio signal because of device bssid at time ts: A REVOIR dans la partie ESP

void beep(long ts, String name) {
  Serial.println("At " + String(ts) + ": " + name + ": beep !");
}

/*
Insert device not yet in surrounding. Should also generates a new record in history
*/
void insert_meet(String name, long rssi, long is_suspect, long ts) {  
  surround_names[surround_size] = name;
  surround_RSSIs[surround_size] = rssi;
  surround_is_suspect[surround_size] = is_suspect;
  surround_timestamp[surround_size] = ts;
  surround_last_timestamp[surround_size] = ts;
  surround_size++;
  total_meet_count++;
}

/*
Return index of surround associated with name. 
Return -1, if name not found in surround
*/
long find_meet(String name) {
  long i;
  for(i = 0; i < surround_size; i++) {
    if ( surround_names[i] == name )
      return i;
  }
  return -1;
}

/*
Update surround with index meet_index
*/
void update_meet(long meet_index, long rssi, long ts) {
  surround_RSSIs[meet_index] = rssi;
  surround_last_timestamp[meet_index] = ts;
}

/*
Remove surround items that have not been active since remove_duration
*/
void cleanup_surround(long ts) {
  long i, removed;
  // Serial.println("Cleanup !");
  removed = 1;
  while (removed != -1) {
    removed = -1;   
    for(i = 0; i < surround_size; i++) {
        if ( ts - surround_last_timestamp[i] > remove_duration) {
          removed = i;
          break;
            }
        }
        if (removed != -1) {
            //logSDCard(surround_names[removed],String day,String hour,String minute, String ms)
            for(i = removed; i < surround_size; i++) {
                surround_names[i] = surround_names[i + 1];
                surround_RSSIs[i] = surround_RSSIs[i + 1];
                surround_timestamp[i] = surround_timestamp[i + 1];
                surround_last_timestamp[i] = surround_last_timestamp[i + 1];
            }
            surround_size -= 1;
        } 
  }
}

/*
Process a slave notification
*/
void process_meet_notification(String name, long rssi, long is_suspect, long ts) {
  double dist;
  long meet_index;
  
  meet_index = find_meet(name);
  if ( meet_index == -1 ) {
    insert_meet(name, rssi, is_suspect, ts);
    meet_index = surround_size - 1;
  }
  else update_meet(meet_index, rssi, ts);

  if (rssi > -50)
      beep(ts, surround_names[meet_index]);
}

long init_surround() {
  long i;
  for(i = 0; i < 100; i++) {
    surround_names[i] = "";
    surround_RSSIs[i] = 0;
    surround_is_suspect[i] = 0;
    surround_timestamp[i] = 0;
    surround_last_timestamp[i] = 0; 
  }
}

 
