#include "ESP32_COVID.h"
#include "surround_management.h"
#include "struct_esp.h"
#include "Var.h"

//------------------------------------------------------------------------------------------------------- 
// Handler Device capté BLE

class MyAdvertisedDeviceCallbacks: public BLEAdvertisedDeviceCallbacks 
{
  void onResult(BLEAdvertisedDevice advertisedDevice) {
    String name = String(advertisedDevice.getName().c_str());
    String s = String(advertisedDevice.getManufacturerData().c_str());
    unsigned char data[4];
    float sig_RSSI=advertisedDevice.getRSSI();
    s.getBytes(data, 4);
    int i;

    if(name.substring(0,9)==String("FacteurCOVID").substring(0,9)){
      for(i=0;i<cpt_scan;i++)
      {
        if(scan_name[i]==name){
          return;
        }
      }
      
      scan_name[cpt_scan]=name;
      cpt_scan++;

      long sig_RSSI_filtree=filtre(sig_RSSI);
      if(affichage_RSSI){
        Serial.print(name + ": " + sig_RSSI_filtree);
      }
      if(affichage_Distance){
        Serial.print(" - distance Ian : " + String(distance(sig_RSSI_filtree)));
      }
     
      process_meet_notification(name, sig_RSSI_filtree, 0, millis() - init_time);  // name, RSSI, is_suspect, time*********************************************************
     
      screen(sig_RSSI); // affichage sur l'écran OLED
    }

    if(t+scan_duration<millis()) //Scan_duration représente le temps de scan en ms, défini comme une variable globale
    {
          pBLEScan->stop();         
    }   
  }
};

//----------------------------------------------------------------------------------------
// setup

void setup() {
  Serial.begin(115200);
  
  chipid=ESP.getEfuseMac();//The chip ID is essentially its MAC address(length: 6 bytes).

  char uuid[13];
  snprintf(uuid, 13, "-%04X%08X", (uint16_t)(chipid>>32), (uint32_t)chipid);
  nom_unique = &uuid[0];
  delay(3000);
  Serial.println("\n" + String(nom_unique));
  
  init_time = millis();
  strcpy(unique_name,nom_unique);
  _prev_data=0;//On défini la data précedente comme = à 0
 
  BLEDevice::init("");//pas utile de mettre un nom, dans notre cas il ne sera pas visible
  
  init_serv();//On initialise le mode serveur de l'ESP
  init_client();//On initialise le mode client de l'ESP, possiblilité de régler un peu la conso dans cette fonction

  init_surround();
}

//-----------------------------------------------------------------------------------------------
// void loop

void loop() {
  if(_prev_data!=_data) //La donnée à été modifié par une rencontre, il faut donc changer le advertising
  {
      advertise();//On lance l'advertise avec la donnée _data
      _prev_data=_data;  
  }
  scan(); 

  cleanup_surround(millis());
  
  Serial.print("\n" + String(surround_size) + " items in surrounding ");
  for(int i = 0; i < surround_size; i++) 
    Serial.print(" / " + surround_names[i] + ": " + String(surround_RSSIs[i]));
    
  Serial.println("\n" + String(total_meet_count) + " meets");
  delay(500);
}


//*********************************************Code Serveur**********************************************************
//****************************************************************************************************************

void init_serv() //initialise le module serveur du BLE, ne prend pas d'arguments, ne retourne rien
{
  //Code ajoute du serveur
  BLEServer *pServer = BLEDevice::createServer();
  pAdvertising = pServer->getAdvertising();
  advert.setName("LocalNameESP32"); //Local name of the ESP32
  pAdvertising->setAdvertisementData(advert);
  pAdvertising->start();
  //Fin ajout

}



void advertise() //Change le advertising du BLE, utilise la variable globale _data et la code manufactureur (man_code), ne return rien
{
  //Code ajouté du serveur
  char nom[50]="FacteurCOVID";
  strcat(nom, unique_name); // On concatène les deux chaines ensemble "Test" est le descriptif après FacteurCOVID. Il peut être défini par un char* ailleurs (genre setup)
    BLEAdvertisementData scan_response; //We create a package of response
    setManData(scan_response, man_code); //Mise en forme de la donnee
    scan_response.setName(nom); //We give a name to our ESP32 (scan)
    pAdvertising->stop(); //Stop advertising to change value
    pAdvertising->setScanResponseData(scan_response);
    
    pAdvertising->start();
  //Fin code ajouté

}


//---------------------------------------------function that adds manufacturer code at the beginning ------------------------
//-----------------------------------Mise en forme de la data-------------------------------------------------------------
void setManData(BLEAdvertisementData &adv, int m_code) //Met en forme la data, prend en entrée une data de type BLEAdvertisementData et un code manufactureur utilise aussi _data Ne return rien
{

  String s;
  char b2 = (char)(m_code >> 8);
  m_code <<= 8;
  char b1 = (char)(m_code >> 8);
  s.concat(b1); //adds manufacturer code at the beginning
  s.concat(b2);
  s.concat((char)_data);//Set data at the end, i should be replace by the data we want to transmit

  unsigned char data[4]; //Pour vérifier les octets envoyés
  s.getBytes(data, 4);
  
  //Serial.print("Data envoyee : ");
  //Serial.println(data[0]);
  //Serial.println(data[1]);
  //Serial.println(data[2]);


  adv.setManufacturerData(s.c_str());
  //int data  = (s.substring(0,1)).toInt();
  //Serial.print("Donnee theoriquement envoyee= "); //Just to check the value of i
//  Serial.println(String(i));

  //Serial.print("String envoye: "); //To check the data we wrote
  //Serial.println(s);

//  adv.setManufacturerData(s.c_str());
  
}

//*********************************************Code Client**********************************************************
//****************************************************************************************************************


//-----------------------------------------------Initialisation du code client-------------------------------
void init_client() //Initialise le module clien du BLE, pas d'arguments, ne return rien
{
  pBLEScan = BLEDevice::getScan(); //create new scan
  pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks(),true);
  pBLEScan->setActiveScan(true); //active scan uses more power, but get results faster
}

//----------------------------------------------------Fonction scan utilisée dans le main-----------------------------
void scan() //lance un scan pendant une duree scan_duration, ne return rien
{
  //Serial.println("Scanning ..");
  int i=0;
  for(i=0;i<10;i++)
  {
    scan_name[i]="";
  }
  cpt_scan=0;
  t=millis();
  BLEScanResults foundDevices = pBLEScan->start(scanTime); //scanTime inutile finalement
  //Serial.print("Devices found: ");
  //Serial.println(foundDevices.getCount());
  //Serial.println("Scan done!"); 
}
