#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>

//*************************************************************Class esp projet****************************
//**********************************************************************************************************
class ESP32_projectCovid //La nouvelle class (genre comme une structure, un nouveau type de donnée)
{
public: //Tout ce qui est dans la partie public est accessible de partout dans le code
  ESP32_projectCovid(String _n,unsigned char _d)//Lorsqu'on va creer une nouvelle variable du type ESP32_projectCovid, c'est cette fonction qui va être appelée*
  {
    _name=_n;//
    _data=_d;//
  }
  ESP32_projectCovid()//Enfin ça depend des paramètres, celle la peut aussi s'appeler
  {
    _name="404"; //Nom de base inscrit si aucun nom n'est passé en paramètre
    unsigned char dat[1];//Un peu de magouillage
    String(0x00).getBytes(dat, 1);//
    _data=dat[0];//
  }
  void setname(String _n){_name=_n;}//Pour pouvoir changer le nom en dehors des fonctions de cette class
  void setdata(unsigned char _d){_data=_d;}//Pour pouvoir changer la data en dehors des fonctions de cette class
  String getname(){return _name;}//Pour pouvoir lire le nom en dehors des fonctions de cette class
  int getdata(){return _data;}//Pour pouvoir lire la data en dehors des fonctions de cette class
  bool appartient(ESP32_projectCovid *tab, int n) //Fonction qui permet de regarder si une variable de type ESP32_projectCovid appartient à un tab de ESP32_projectCovid
  {
    int i=0; 
    for(i;i<n;i++)
    {
        if((tab[i]._name==_name)){return true;} //Vu que le nom sera unique, si on trouve le même nom c'est que le ESP est deja enregistré
    }
    return false;//Si on a rien trouvé c'est que c'est pas dedans
  }
  void affiche()//On affiche un element de type ESP32_projectCovid
  {
    //Serial.println("Name :"+_name); //Juste un petit affichage du nom
    //Serial.print("Data :"); //Affichage de la data
    //Serial.println(_data);
  }

  void ajout_esp(ESP32_projectCovid *tab,int n)//Fonction qui ajoute un type ESP32_projectCovid au tableau (s'il y a de la place)
  {
    int i=0;
    bool place=false;//Sert à savoir s'il y a de la place, en théorie on peut faire une fonction pour ça, ce sera plus propre, mais la flemme
    for(i;i<n;i++)
    {
      if(tab[i]._name=="404")//Si un nom correspond a 404, c'est le nom de base, il reste donc de la place
      {
        Serial.println("Il reste de la place dans le tableau en mémoire, on ajoute donc");
        place = true;
        break;
      }
    }
    if(place) //S'il y a de la place c'est nice
    {
      tab[i].setname(_name);
      tab[i].setdata(_data);
    }
    else{Serial.println("Il n'y a pas de place sorry");} //Sinon, t'avais qu'a mieux gérer ta mémoire
    
  }
private: //Ces données sont seulement accessibles par des fonctions de la classe ESP32_projectCovid
  String _name;  //Le nom du ESP, qui de base est 404
  unsigned char _data; //La data
};
//------------------------------------------Fonction affichage liée à la classe ESP32_projet--------------------------------------
void affiche(ESP32_projectCovid *tab,int n)//l'objectif est d'affiche les elements déclarés dans le tab
{

   Serial.println("On affiche le tab de la classe :");
  int i=0;
  for(i;i<10;i++)//on parcourt le tab inscrit en memoire flash
  {
    if(tab[i].getname()==String(404))//404 est le nom rempli dans le tab lorsqu'il y a rien
    {
      break; 
    }
    tab[i].affiche();// Utilisation de la fonction affiche de la classe correspondante
  }
}

